import { NavLink, Outlet, Link } from 'react-router-dom';

function Nav() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Manufacturer
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/manufacturers">List of Manufacturers</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/manufacturers/create">Create Manufacturer</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Model
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/models">List of Models</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/models/create">Create Model</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Automobile
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/automobile">List of Automobiles</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/automobile/create">Create Automobile</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Technician
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  {/* <li><Link className="dropdown-item" activeclassname="inactive" to="technicians">List Of Technicians</Link></li> */}
                  <li><Link className="dropdown-item" activeclassname="inactive" to="technician/create">Create Technician</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales Person
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  {/* <li><Link className="dropdown-item" activeclassname="inactive" to="salesperson">List Of Sales Persons</Link></li> */}
                  <li><Link className="dropdown-item" activeclassname="inactive" to="salesperson/create">Create Sales Person</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  New Customer
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  {/* <li><Link className="dropdown-item" activeclassname="inactive" to="customer">List Of Customers</Link></li> */}
                  <li><Link className="dropdown-item" activeclassname="inactive" to="customer/create">Create Customer</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales Record
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/salesrecord">List of Sale Records</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="salesrecord/create">Create Sales Record</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="saleshistory">Sales History by Sales Person</Link></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" id="navbarDropdownCars" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Service Appointment
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownCars">
                  <li><Link className="dropdown-item" activeclassname="inactive" to="/appointment/list">List of Service Appointments</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="appointment/create">Create Service Appointment</Link></li>
                  <li><Link className="dropdown-item" activeclassname="inactive" to="servicehistory">Service History by VIN</Link></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <Outlet />
    </div>
  )
}

export default Nav;
