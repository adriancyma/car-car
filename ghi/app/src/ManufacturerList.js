import React, { useEffect, useState } from "react";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  const getManufacturer = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    const data = await response.json();
    setManufacturers(data.manufacturers);
  };
  // delete function is extra
  useEffect(() => {
    getManufacturer();
  }, []);

  return (
    <div>
      <h1 className="text-center">Manufacturers</h1>
      <table className="table table-striped">
        <thead className="text-center">
          <tr className="header">
            <th>Name</th>
          </tr>
        </thead>
        <tbody className="text-center">
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.href}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ManufacturerList;
