import React from "react";
import { useEffect, useState } from "react";

const AutoMobileList = () => {
	const [autos, setAutos] = useState([]);

	const fetchModels = async () => {
		const modelUrl = "http://localhost:8100/api/automobiles/";

		const response = await fetch(modelUrl);

		if (response.ok) {
			const data = await response.json();
			setAutos(data.autos);
		} else {
			console.error(response);
		}
	};

	useEffect(() => {
		fetchModels();
	}, []);

	return (
		<>
			<div className="d-flex mt-5 mb-5 justify-content-center">
				<h1>Automobiles</h1>
			</div>
			<table className="table table-striped">
				<thead className="text-center">
					<tr>
						<th>Vin</th>
						<th>Color</th>
						<th>Year</th>
						<th>Model</th>
						<th>Manufacturer</th>
					</tr>
				</thead>
				<tbody className="text-center">
					{autos.map((auto) => {
						return (
							<tr key={auto.id}>
								<td>{auto.vin}</td>
								<td>{auto.color}</td>
								<td>{auto.year}</td>
								<td>{auto.model.name}</td>
								<td>{auto.model.manufacturer.name}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
};

export default AutoMobileList;
