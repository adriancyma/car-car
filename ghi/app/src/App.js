import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ModelList from "./VehicleModelList";
import ModelCreateForm from "./ModelForm";
import ManufacturerList from "./ManufacturerList";
import NewManufacturerForm from "./ManufacturerForm";
import AutoMobileList from "./AutoMobileList";
import NewAutomobileForm from "./AutomobileForm";
import SalesPersonCreateForm from "./SalesPersonForm";
import CreateCustomerForm from "./CustomerForm";
import SalesRecordCreateForm from "./SalesRecordForm";
import SalesRecordsList from "./SalesRecordList";
import SalesHistoryList from "./SalesHistory";
import NewTechnicianForm from "./TechnicianForm";
import NewAppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import ServiceHistory from "./ServiceHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route
            path="manufacturers/create"
            element={<NewManufacturerForm />}
          />
          <Route path="models" element={<ModelList />} />
          <Route path="models/create" element={<ModelCreateForm />} />
          <Route path="automobile" element={<AutoMobileList />} />
          <Route path="automobile/create" element={<NewAutomobileForm />} />
          <Route
            path="salesperson/create"
            element={<SalesPersonCreateForm />}
          />
          <Route path="customer/create" element={<CreateCustomerForm />} />
          <Route path="salesrecord" element={<SalesRecordsList />} />
          <Route
            path="salesrecord/create"
            element={<SalesRecordCreateForm />}
          />
          <Route path="saleshistory" element={<SalesHistoryList />} />
          <Route path="appointment/create" element={<NewAppointmentForm />} />
          <Route path="appointment/list" element={<AppointmentList />} />
          <Route path="technician/create" element={<NewTechnicianForm />} />
          <Route path="servicehistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
